-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2015-10-19 06:25:50
-- 服务器版本： 5.5.16
-- PHP Version: 5.4.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- 表的结构 `l_api`
--

CREATE TABLE IF NOT EXISTS `l_api` (
  `api_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_listsort` int(11) NOT NULL,
  `api_status` tinyint(4) NOT NULL,
  `api_lang` tinyint(4) NOT NULL,
  `api_link` varchar(255) NOT NULL,
  `api_createtime` int(11) NOT NULL,
  `api_title` varchar(255) NOT NULL,
  PRIMARY KEY (`api_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `l_dologin`
--

CREATE TABLE IF NOT EXISTS `l_dologin` (
  `dologin_id` int(11) NOT NULL AUTO_INCREMENT,
  `dologin_listsort` int(11) NOT NULL,
  `dologin_status` tinyint(4) NOT NULL,
  `dologin_lang` tinyint(4) NOT NULL,
  `dologin_url` varchar(255) NOT NULL,
  `dologin_createtime` int(11) NOT NULL,
  `dologin_account` varchar(255) NOT NULL,
  `dologin_mark` varchar(255) NOT NULL,
  `dologin_session` text NOT NULL,
  PRIMARY KEY (`dologin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `l_field`
--

CREATE TABLE IF NOT EXISTS `l_field` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `field_name` varchar(128) NOT NULL,
  `display_name` varchar(128) NOT NULL,
  `field_type` varchar(128) NOT NULL,
  `field_option` text NOT NULL,
  `field_default` varchar(128) NOT NULL,
  `field_required` tinyint(4) NOT NULL,
  `field_explain` varchar(256) NOT NULL,
  `field_listsort` int(11) NOT NULL,
  `field_list` enum('0','1') NOT NULL DEFAULT '0' COMMENT '是否显示于列表 0:不显示 1:显示',
  `field_status` tinyint(4) NOT NULL,
  PRIMARY KEY (`field_id`),
  UNIQUE KEY `modle_id` (`model_id`,`field_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='字段列表' AUTO_INCREMENT=81 ;

--
-- 转存表中的数据 `l_field`
--

INSERT INTO `l_field` (`field_id`, `model_id`, `field_name`, `display_name`, `field_type`, `field_option`, `field_default`, `field_required`, `field_explain`, `field_listsort`, `field_list`, `field_status`) VALUES
(57, 14, 'status', '状态', 'radio', '{"\\u7981\\u7528":"0","\\u542f\\u7528":"1"}', '1', 1, '', 100, '1', 1),
(58, 14, 'listsort', '排序', 'text', '', '', 0, '', 98, '0', 0),
(59, 14, 'createtime', '创建时间', 'date', '', '', 0, '', 99, '0', 1),
(60, 14, 'name', '所属者', 'text', '', '', 1, '', 1, '1', 1),
(61, 14, 'address', '物理地址', 'text', '', '', 1, '本地址由软件生成，请勿随意变更。', 2, '1', 1),
(62, 14, 'verify', '匹配值', 'text', '', '', 1, '本值由程序自动生成，请勿随意修改。', 3, '1', 1),
(63, 15, 'status', '状态', 'radio', '{"\\u7981\\u7528":"0","\\u542f\\u7528":"1"}', '1', 1, '', 100, '1', 1),
(64, 15, 'listsort', '排序', 'text', '', '', 0, '', 0, '1', 1),
(65, 15, 'createtime', '创建时间', 'date', '', '', 0, '', 99, '0', 0),
(66, 15, 'title', '接口名称', 'text', '', '', 1, '登陆器显示的网站列表名称', 1, '1', 1),
(67, 15, 'link', '接口地址', 'text', '', '', 1, '验证用户输入帐号和密码是否正确的登录接口。具体请参考PESCMS文档说明', 2, '0', 1),
(70, 16, 'status', '状态', 'radio', '{"\\u672a\\u7528":"0","\\u5df2\\u7528":"1"}', '1', 1, '', 100, '1', 1),
(71, 16, 'listsort', '排序', 'text', '', '', 0, '', 98, '0', 1),
(72, 16, 'createtime', '创建时间', 'date', '', '', 1, '', 99, '1', 1),
(73, 16, 'account', '登录帐号', 'text', '', '', 1, '', 1, '1', 1),
(74, 16, 'mark', '登录标记', 'text', '', '', 1, '', 3, '0', 1),
(75, 16, 'session', 'session', 'textarea', '', '', 1, '', 4, '0', 1),
(76, 17, 'status', '状态', 'radio', '{"\\u7981\\u7528":"0","\\u542f\\u7528":"1"}', '1', 1, '', 100, '0', 1),
(78, 17, 'createtime', '创建时间', 'date', '', '', 0, '', 99, '0', 0),
(79, 17, 'no', '最新版本号', 'text', '', '', 1, '程序是检测最后一次提交的版本号', 1, '1', 1),
(80, 17, 'link', '新版下载地址', 'text', '', '', 1, '填写对应版软件下载地址', 2, '0', 1);

-- --------------------------------------------------------

--
-- 表的结构 `l_mac`
--

CREATE TABLE IF NOT EXISTS `l_mac` (
  `mac_id` int(11) NOT NULL AUTO_INCREMENT,
  `mac_listsort` int(11) NOT NULL,
  `mac_status` tinyint(4) NOT NULL,
  `mac_lang` tinyint(4) NOT NULL,
  `mac_url` varchar(255) NOT NULL,
  `mac_createtime` int(11) NOT NULL,
  `mac_name` varchar(255) NOT NULL,
  `mac_address` varchar(255) NOT NULL,
  `mac_verify` varchar(255) NOT NULL,
  PRIMARY KEY (`mac_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `l_menu`
--

CREATE TABLE IF NOT EXISTS `l_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(128) NOT NULL,
  `menu_pid` int(11) NOT NULL,
  `menu_icon` varchar(128) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_listsort` tinyint(100) NOT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `menu_pid` (`menu_pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='菜单列表' AUTO_INCREMENT=56 ;

--
-- 转存表中的数据 `l_menu`
--

INSERT INTO `l_menu` (`menu_id`, `menu_name`, `menu_pid`, `menu_icon`, `menu_url`, `menu_listsort`) VALUES
(1, '基础设置', 0, 'am-icon-tachometer', '', 0),
(8, '后台菜单', 1, 'am-icon-align-justify', 'Admin-Index-menuList', 1),
(11, '清空缓存', 1, 'am-icon-refresh', 'Admin-Index-clear', 0),
(50, '物理地址', 1, 'am-icon-location-arrow', 'Admin-Mac-index', 4),
(51, '接口地址', 1, 'am-icon-book', 'Admin-Api-index', 2),
(52, '登录记录', 1, 'am-icon-key', 'Admin-Dologin-index', 3),
(54, '登陆器版本', 9, 'am-icon-file', 'Admin-Version-index', 0),
(55, '登陆器版本', 1, 'am-icon-sort-numeric-asc', 'Admin-Version-index', 5);

-- --------------------------------------------------------

--
-- 表的结构 `l_model`
--

CREATE TABLE IF NOT EXISTS `l_model` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(128) NOT NULL,
  `lang_key` varchar(128) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_search` tinyint(11) NOT NULL COMMENT '允许搜索',
  `model_attr` tinyint(1) NOT NULL COMMENT '模型属性 1:前台(含前台) 2:后台',
  PRIMARY KEY (`model_id`),
  UNIQUE KEY `model_name` (`model_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='模型列表' AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `l_model`
--

INSERT INTO `l_model` (`model_id`, `model_name`, `lang_key`, `status`, `is_search`, `model_attr`) VALUES
(14, 'Mac', '物理地址', 1, 1, 1),
(15, 'Api', '接口地址', 1, 1, 1),
(16, 'Dologin', '登录记录', 1, 1, 2),
(17, 'Version', '登陆器版本', 1, 0, 2);

-- --------------------------------------------------------

--
-- 表的结构 `l_option`
--

CREATE TABLE IF NOT EXISTS `l_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `option_range` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统选项' AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `l_option`
--

INSERT INTO `l_option` (`id`, `option_name`, `name`, `value`, `option_range`) VALUES
(1, 'sitetitle', '程序标题', 'PESCMS Login', ''),
(8, 'fieldType', '表单类型', '{"category":"\\u5206\\u7c7b","text":"\\u5355\\u884c\\u8f93\\u5165\\u6846","radio":"\\u5355\\u9009\\u6309\\u94ae","checkbox":"\\u590d\\u9009\\u6846","select":"\\u5355\\u9009\\u4e0b\\u62c9\\u6846","textarea":"\\u591a\\u884c\\u8f93\\u5165\\u6846","editor":"\\u7f16\\u8f91\\u5668","thumb":"\\u7565\\u7f29\\u56fe","img":"\\u4e0a\\u4f20\\u56fe\\u7ec4","file":"\\u4e0a\\u4f20\\u6587\\u4ef6","date":"\\u65e5\\u671f"}', 'Miscellaneous'),
(14, 'upload_img', '图片格式', '["jpg","jpge","bmp","gif","png"]', 'upload'),
(15, 'upload_file', '文件格式', '["zip","rar","7z","doc","docx","pdf","xls","xlsx","ppt","pptx","txt"]', 'upload');

-- --------------------------------------------------------

--
-- 表的结构 `l_user`
--

CREATE TABLE IF NOT EXISTS `l_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_mail` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `user_status` tinyint(4) NOT NULL,
  `user_createtime` int(11) NOT NULL,
  `user_last_login` int(11) NOT NULL,
  `user_department_id` varchar(255) NOT NULL,
  `user_head` text NOT NULL COMMENT '用户头像',
  `user_ey` int(11) unsigned NOT NULL COMMENT '用户的ey值',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_account` (`user_account`),
  UNIQUE KEY `user_mail` (`user_mail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `l_version`
--

CREATE TABLE IF NOT EXISTS `l_version` (
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_status` tinyint(4) NOT NULL,
  `version_lang` tinyint(4) NOT NULL,
  `version_url` varchar(255) NOT NULL,
  `version_createtime` int(11) NOT NULL,
  `version_no` varchar(255) NOT NULL,
  `version_link` varchar(255) NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `l_version`
--

INSERT INTO `l_version` (`version_id`, `version_status`, `version_lang`, `version_url`, `version_createtime`, `version_no`, `version_link`) VALUES
(1, 1, 0, '/?m=Version&a=view&id=1', 1442644620, '1.0', 'http://www.pescms.com/d/v/10/2.html');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
